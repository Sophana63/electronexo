// Modules to control application life and create native browser window
const {app, BrowserWindow, ipcMain, Notification} = require('electron')
let cron = require('node-cron');
const path = require('path');
const fetch = require('electron-fetch').default;
let loginWindow = null
let loadWindow = null

function createWindow () {
  // Create the browser window.
  loginWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      // preload: path.join(__dirname, 'preload.js')
      nodeIntegration: true,
      contextIsolation: false  
    }
  })

  // and load the index.html of the app.

  loginWindow.maximize();
  loginWindow.loadFile('page/login/login.html')  
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.




function showNotification2 () {
  let notif = new Notification({ title: 'Card', body: 'Pas de nouvelle carte' }).show();
  return notif;
}

app.whenReady().then(() => {
  createWindow()
  //showNotification()
  cron.schedule('* * * * *', () => {
    console.log('running every minute to 1 from 5');

    

    function getDateMinus5MinutesString() {
      let date = new Date();
      let newDate = new Date(date.getTime() - 5 * 60000);
      return newDate.toLocaleDateString('fr-CA') + "%20" + newDate.getHours() + ":" + newDate.getMinutes() + ":" + newDate.getSeconds();
    }

    let dateString = getDateMinus5MinutesString();

    const NOTIFICATION_TITLE = 'Card'    
    
    //showNotification()
    fetch('http://127.0.0.1:8000/api/cards?createdAt[after]=' + dateString, {
        method: 'GET',
        headers: {
        'Accept': 'application/json'
        },        
    }).then((response) => {
    return response.json();
    }).then((data) => {
        if (data.length > 0) {
          let i = 0;
          for (i = 0; i < data.length; i++) {
            new Notification({ title: NOTIFICATION_TITLE, body: 'Nouvelle carte créée : ' + data[i].name + ' le ' + data[i].createdAt}).show();         
          }
        }
        
    
    })
  });

  
  
    

  app.on('activate', function () {    
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})


// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

ipcMain.on('loader_fenetre', (event, arg) => {
  console.log('load')
  function createWindow2 () {
    loadWindow = new BrowserWindow({
      width: 200,
      height: 200,
      frame: false,
      webPreferences: {
        nodeIntegration: true,
        contextIsolation: false  
      }
    })
    
    loadWindow.loadFile('page/login/loader.html')

  }
  createWindow2 ();
});

ipcMain.on('fermer_fenetre', (event, arg) => {
  loadWindow.close();
  loginWindow.loadFile('page/dashboard/dashboard.html')
});

ipcMain.on('stat_visite', (event, arg) => {
  loginWindow.loadFile('page/visite/visite.html')
});

ipcMain.on('stat_card', (event, arg) => {
  loginWindow.loadFile('page/card/card.html')
});
