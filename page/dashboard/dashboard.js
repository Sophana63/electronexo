const {ipcRenderer} = require('electron');
let jwt_decode = require('jwt-decode');

let year = new Date().getFullYear();
let stringFetch = 'http://127.0.0.1:8000/visite/' + year
let btnStat = document.getElementById('statVisite');
let btnStatCard = document.getElementById('statCard');

btnStat.addEventListener('click', () => {
    ipcRenderer.send('stat_visite');
})
btnStatCard.addEventListener('click', () => {
    ipcRenderer.send('stat_card');
})


// console.log(year)

fetch(stringFetch, {
        method: 'GET',
       
    }).then((response) => {
    return response.json();
    }).then((data) => {
        //ipcRenderer.send('fermer_fenetre');
        console.log(data.visites)
    if (data) {        
        let targetDiv = document.getElementById('tabVisite');
        var tableauMois = new Array(
            'Janvier',
            'Février',
            'Mars',
            'Avril',
            'Mai',
            'Juin',
            'Juillet',
            'Août',
            'Septembre',
            'Octobre',
            'Novembre',
            'Décembre'
        );

        let table = document.createElement('table');
        let thead = document.createElement('thead');
        let tbody = document.createElement('tbody');

        let row_1 = document.createElement('tr');
        let heading_1 = document.createElement('th');
        heading_1.innerHTML = "Mois";
        let heading_2 = document.createElement('th');
        heading_2.innerHTML = "Visites";

        row_1.appendChild(heading_1);
        row_1.appendChild(heading_2);
        thead.appendChild(row_1);

        let totalVisites = 0;

        for (let i =0; i <data.visites.length; i++) {
            totalVisites = totalVisites + data.visites[i].visited
        }

        let moyenneVisite = totalVisites / data.visites.length;

        data.visites.forEach((el) => {
            let row_2 = document.createElement('tr');
            let row_2_data_1 = document.createElement('td');
            row_2_data_1.innerHTML = tableauMois[el.mois - 1] ;
            let row_2_data_2 = document.createElement('td');
            if (el.visited > moyenneVisite) {
                row_2_data_2.style.color = "red"
                row_2_data_2.innerHTML = el.visited;
            } else {
                row_2_data_2.innerHTML = el.visited;
            }
            

            row_2.appendChild(row_2_data_1);
            row_2.appendChild(row_2_data_2);
            tbody.appendChild(row_2);

        })

        table.appendChild(thead);
        table.appendChild(tbody);

        // Adding the entire table to the body tag
        targetDiv.appendChild(table);

        targetDiv.insertAdjacentHTML( 'beforeend', '<p> Total des visites: ' + totalVisites + '</p>');
        targetDiv.insertAdjacentHTML( 'beforeend', '<p> Moyenne des visites par mois: ' + moyenneVisite + '</p>');

        
    }
    
    
})