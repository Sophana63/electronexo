const {ipcRenderer} = require('electron');
let jwt_decode = require('jwt-decode');
let btnSubmit = document.getElementById('submitWindows');

btnSubmit.addEventListener('click', () => {
    // ipcRenderer.send('fermer_fenetre');
    let email =  document.getElementById("email").value;
    let mdp =  document.getElementById("mdp").value
    
    ipcRenderer.send('loader_fenetre');

    fetch('http://127.0.0.1:8000/api/login_check', {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json'
        },
        body: JSON.stringify({
        email: 'admin@admin.fr',
        password: 'admin'
        })
    }).then((response) => {
    return response.json();
    }).then((data) => {
        ipcRenderer.send('fermer_fenetre');
    if (data.code) {
        let targetDiv = document.getElementById('msgErr');
    targetDiv.insertAdjacentHTML('afterend', data.message);
    }
    if (data.token) {
        let tokens = jwt_decode(data.token);
        console.log(tokens);
        let rolesUser = tokens.roles;
        if (!rolesUser.includes('ROLE_ADMIN')) {
            targetDiv.insertAdjacentHTML('afterend', "Vous n'êtes pas un admin");
        }
        
    }
    
    })

})



// ipcRenderer.on('exemple_reply', (event, arg) => {
//     console.log(arg);
// });