let targetDiv = document.getElementById('divVisite')
let titleDiv = document.getElementById('titleVisite')
let year = new Date().getFullYear();
let stringFetch = 'http://127.0.0.1:8000/card/';
let newYear = year;

for (let i = 0; i < 6; i++) {
    let btnStat = document.getElementById('n' + i);
    btnStat.addEventListener('click', () => {
        targetDiv.innerHTML = "";
        titleDiv.innerHTML = "";

        newYear = year - i;
        stringFetch = 'http://127.0.0.1:8000/card/' + newYear

        fetch(stringFetch, {
            method: 'GET',
           
        }).then((response) => {
        return response.json();
        }).then((data) => {
            //ipcRenderer.send('fermer_fenetre');
            console.log(data.cards)
        if (data) {        
            titleDiv.insertAdjacentHTML( 'beforeend', '<p> Année de création: ' + newYear  + '</p>');
            var tableauMois = new Array(
                'Janvier',
                'Février',
                'Mars',
                'Avril',
                'Mai',
                'Juin',
                'Juillet',
                'Août',
                'Septembre',
                'Octobre',
                'Novembre',
                'Décembre'
            );
    
            let table = document.createElement('table');
            let thead = document.createElement('thead');
            let tbody = document.createElement('tbody');
    
            let row_1 = document.createElement('tr');
            let heading_1 = document.createElement('th');
            heading_1.innerHTML = "Mois";
            let heading_2 = document.createElement('th');
            heading_2.innerHTML = "Nb créé";
    
            row_1.appendChild(heading_1);
            row_1.appendChild(heading_2);
            thead.appendChild(row_1);
    
            let totalVisites = 0;

            data.cards.forEach((elem) => {
                totalVisites = totalVisites + elem.created
            })
    
            // for (let i =0; i <data.cards.length; i++) {
            //     totalVisites = totalVisites + data.cards[i].created
            // }
    
            let moyenneVisite = totalVisites / data.cards.length;
    
            data.cards.forEach((el) => {
                let row_2 = document.createElement('tr');
                let row_2_data_1 = document.createElement('td');
                row_2_data_1.innerHTML = tableauMois[el.mois - 1] ;
                let row_2_data_2 = document.createElement('td');
                if (el.created > moyenneVisite) {
                    row_2_data_2.style.color = "red"
                    row_2_data_2.innerHTML = el.created;
                } else {
                    row_2_data_2.innerHTML = el.created;
                }
                
    
                row_2.appendChild(row_2_data_1);
                row_2.appendChild(row_2_data_2);
                tbody.appendChild(row_2);
    
            })
    
            table.appendChild(thead);
            table.appendChild(tbody);
    
            // Adding the entire table to the body tag
            targetDiv.appendChild(table);
    
            targetDiv.insertAdjacentHTML( 'beforeend', '<p> Total des visites: ' + totalVisites + '</p>');
            targetDiv.insertAdjacentHTML( 'beforeend', '<p> Moyenne des visites par mois: ' + moyenneVisite + '</p>');
    
            
            }    
        
        })
    })
}

